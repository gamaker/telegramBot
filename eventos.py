from datetime import datetime, timedelta
import caldav
from caldav.elements import dav, cdav
from icalendar import Calendar, Event
import config

def getEventos(texto, dias):
    # Config
    user = config.user
    pas = config.pas
    url = "https://"+user+":"+pas+"@"+config.server+"/remote.php/dav/calendars/"+user+"/"

    #Calendaios
    client = caldav.DAVClient(url)
    principal = client.principal()
    calendars = principal.calendars()
    if len(calendars) > 0:
        calendar = calendars[config.calendar]
        print ("Using calendar", calendar)

        print ("Buscando eventos")
        hoy = datetime.now()
        mes = timedelta(days=dias)
        results = calendar.date_search(
            hoy, hoy+mes)

        eventParser= texto+'\n\n'
        for event in reversed(results):
            #print ("Found", event)
            contenidoEvento = event.data
            gcal = Calendar.from_ical(contenidoEvento)
            for component in gcal.walk():
                if component.name == "VEVENT":
                    desc = component.get('summary')
                    startDate = component.get('dtstart').dt.strftime('[ %d/%m/%Y ')+str(int(component.get('dtstart').dt.strftime('%H'))+2)+":"+component.get('dtstart').dt.strftime('%M ]')
                    #endDate = component.get('dtend').dt.strftime('%d/%m/%Y')
                    #fechaGlobal = component.get('dtstamp').dt.strftime('%d/%m/%Y')
                    eventParser += startDate+"\n - "+desc+"\n\n"
        if len(results) > 0:
            print(eventParser)
            return eventParser
        else:
            print("No hay eventos que mostrar")
            return "No hay eventos que mostrar"