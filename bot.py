while True:
    from telegram.ext import Updater, CommandHandler
    import config

    botToken = config.botToken
    chatId = config.chatId

    def eventos(update, context):
        import eventos
        update.message.reply_text(
            eventos.getEventos("Eventos para el proximo mes", 30))

    updater = Updater(botToken, use_context=True)

    updater.dispatcher.add_handler(CommandHandler('eventos', eventos))

    updater.start_polling()
    updater.idle()